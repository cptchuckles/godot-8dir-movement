extends Label

func _process( _delta ):
    var left  = Input.is_action_pressed("ui_left")  
    var right = Input.is_action_pressed("ui_right") 
    var up    = Input.is_action_pressed("ui_up")    
    var down  = Input.is_action_pressed("ui_down")  

    text = "        up:" + String(up)
    text += "\nleft:" + String(left)
    text += "        right:" + String(right)
    text += "\n      down:" + String(down)
