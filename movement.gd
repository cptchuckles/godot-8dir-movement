extends KinematicBody2D

export var SPEED : float = 200.0
export var SNAPPINESS : float = 0.2
export var STICTION : float = 0.1

export(NodePath) var sprite_handle
onready var hsprite : Sprite = get_node(sprite_handle)

var movement = Vector2.ZERO
var lagturn = Vector2.RIGHT

func _physics_process( _delta ):
	var left  : int = Input.is_action_pressed("ui_left")  as int
	var right : int = Input.is_action_pressed("ui_right") as int
	var up	  : int = Input.is_action_pressed("ui_up")    as int
	var down  : int = Input.is_action_pressed("ui_down")  as int

	var targetmove = Vector2( right-left, down-up ).normalized()

	movement = movement.linear_interpolate(targetmove, STICTION)
	move_and_slide(movement*SPEED)
	
	lagturn = lagturn.slerp(movement, SNAPPINESS)
	hsprite.rotation = lagturn.angle()
